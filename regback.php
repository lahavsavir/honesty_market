<?
$errCode    = empty($_GET['result']) ? 0  : (int) $_GET['result'];
$errMessage = empty($_GET['error'])  ? "" : htmlspecialchars($_GET['error']);
?>

<!DOCTYPE html>
<head>
 <meta charset='utf-8' />
</head>
<body>

<script type='text/javascript'>
	window.top.myApp.mpiEnd(<?=$errCode?>, "<?=$errMessage?>");
</script>

</body>