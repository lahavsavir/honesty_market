<?php $rand = 's2'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Check In</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

	<link rel="stylesheet" href="//code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.css" />
	<link rel="stylesheet" href="./media/css/basic.css?rand=<?=$rand;?>" />
	
	<script src="//code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="//code.jquery.com/mobile/1.4.0/jquery.mobile-1.4.0.min.js"></script>
	<script src="//d3hirvd0ltjtq4.cloudfront.net/wizViewMessengerBridge.js"></script>
	<script src="./media/js/jquery.qr.min.js"></script>
	<script src="./media/js/basic.js?rand=<?=$rand;?>"></script>
</head>
<body>

<div data-role="page" id = "qr">

	<div data-role="header">
	</div>

	<div role="main" class="ui-content disableLeftPadd">
		<p id = "loaderBox"></p>
		<div id = "qrContent"><div id = "whiteOne"></div><div id="qrcode"></div></div>
		<p class = "centerPar dontShow"><span id = "scanText">Scan this code to pay at:</span><br /><span class = "bizName highlight"></span></p>
	</div>

	<div data-role="footer" data-position="fixed" data-tap-toggle="false" class = "alignFooter disableLeftPadd">
		<div id = "currPmethod" class = "disableLeftPadd">
			<span class = "alignedC">CURRENT PAYMENT METHOD:</span>
			<select id = "paymentMethod" data-role="select"></select>
		</div>
		
		<div id = "btnList">
			<button data-role="none" class = "greenBtn" id = "addCreditBtn">Add Payment Method</button>
			<button data-role="none" class = "greenBtn" id = "addCouponBtn">Add Coupon</button>
		</div>
	</div>
	
	<div data-role="popup" id="popupBasic" data-overlay-theme="b" data-dismissible="false">
		<p>
			<img src = "./media/img/logo.png" class = "logoReplace" alt = "" /> <br />
			<div class = "under"></div>
			<h3>Success!</h3>
			<span class = "thanksText">The receipt for this purchase has <br /> been emailed to you!</span>
			<button data-role="none" class = "greenBtn inPop reloadPage">Continue</button>
		</p>
	</div>
	
	<div data-role="popup" id="popupBasicCancel" data-overlay-theme="b" data-dismissible="false">
		<p>
			<img src = "./media/img/logo.png" class="logoReplace" alt = "" /> <br />
			<div class = "under"></div>
			<h3>Oops!</h3>
			<span class = "thanksText">Your order has been cancelled. <br /> Please try again.</span>
			<button data-role="none" class = "greenBtn inPop reloadPage" >Continue</button>
		</p>
	</div>	
	

	
</div>

<div data-role="page" id = "addCoupon">

	<div data-role="header" class = "customHeader">
		<img src = "./media/img/logo.png" class="logoReplace" alt = "" />
	</div>

	<div role="main" class="ui-content">
		<div class = "pageTitle">REDEEM COUPON</div>
		
		<div class = "subContent">
			Coupon Balance: <span class = "theBalance">$0</span> <br />
			<span id = "couponStatus"></span>
			<input type="text" id="promCode" placeholder="Enter your code" value = "" />
			<button data-role="none" class = "greenBtn full" id = "addCouponSubmit" >OK</button>
		</div>
		
		<div id = "couponDesc">* First charges will automatically come from the Coupon, the rest will be from the default payment method you provided.</div>
	</div>
	
</div>

<div data-role="page" id = "addCredit">

	<div data-role="header" class = "customHeader">
		<img src = "./media/img/logo.png" class="logoReplace" alt = "" />
	</div>

	<div role="main" class="ui-content">
		<div id = "mpiText">PAY FOR ANYTHING IN THE HONESTY MARKET WITH WEWORK MOBILE</div>
		<div id = "mpiContainer"></div>
	</div>
	
	<div data-dismissible="true" data-role="popup" id="popupLoader" data-theme="a" data-corners="false" data-transition="slide" data-position-to='window'  data-overlay-theme="b"><h3>Please wait...</h3></div>

	<div data-role="popup" id="popupBasicPayment"  data-overlay-theme="b" data-dismissible="false">
		<p>
			<img src = "./media/img/logo.png" class="logoReplace" alt = "" /> <br />
			<div class = "under"></div>
			<h3>Success!</h3>
			<span class = "thanksText">popup text</span>
			<button data-role="none" class = "greenBtn inPop" id = "closePop">Continue</button>
		</p>
	</div>	
</div>

<script type = "text/javascript">
	$( document ).bind( "mobileinit", function() 
	{
		$.mobile.allowCrossDomainPages = true;
		$.mobile.page.prototype.options.domCache = false;
		$.mobile.silentScroll(0);
	});

	// Reset hash
	window.location.hash = '';
	
	//use https protocol
	if (window.location.protocol != "https:")
	    window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
	
	$(document).ready(function(e) 
	{
		//init app
		myApp = new WeWork();
		myApp.init();
	}); 
</script>

</body>
</html>