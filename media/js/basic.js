/*
	We Work 
	White Label
	Author: Elad.A <elad.a@mycheck.co.il>
*/

function WeWork()
{
	//private vars
	var self				=		this;
	
	//public vars
	this.mpiLoaded 			= 		false;
	this.orderInterval  	= 		4000;
	this.verifyInterval 	=       2000;
	this.myTimer 			= 		null;
	this.verifyTimer  		=       null;
	this.disableAdd 		= 		true;
	this.redirectToCredit 	=     	false;
	this.appType 			=		'Weworkapp';
	this.couponBalance		=		0;
	
	/* end points */
	this.url 				=		"https://" + window.location.host + "/";
	this.mpiUrl 			= 		self.url + 'user/mpi/locale/weWorkApp/?whitelabel=/wwwl';
	
	this.getParameterByName = function(name)
	{
	    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
	    
	    if (results==null)
	       return null;
	    else
	       return results[1] || 0;
	};
	
	this.loginError = function(e , a)
	{
		console.log(a);
	};
	
	this.resultError = function(text)
	{
		$('#loaderBox').html("<h4>" + text + "</h4>").show();
	};	
	
	this.loginResult = function(json)
	{
		/*
			Error Code:
			14 - invalid input
			15 - invalid token
			16 - user does not verified
			17 - user does not near an honesty market
		*/
		
		if (json.ErrorCode > 0)
		{
			if(json.ErrorCode == 17)
				self.resultError("You're not near an Honesty Market right now.");
			else if(json.ErrorCode == 15)
			{
				self.resultError("Invalid token");
				$('.alignFooter').hide();
			}
			else
				self.resultError(json.ReturnString);
				
			if(json.ErrorCode == 16)
			{
				//check user status... status should be verified!
				self.verifyTimer = window.setInterval(self.checkVerify, self.verifyInterval);
			}
			
			return;
		}
		else
		{
			var clientCode = (!json.ClientCode) ? 2140 : json.ClientCode;

			//fill cards list & generate client code
			var select = $('#paymentMethod');
			if(json.PaymentMethods.length == 0)
			{
				$('#qrcode').html('Add Your Payment Method');
				$('#qrcode , #qrContent').addClass('full');
				select.append("<option value='0'>No Payment Method</option>");
				
				//auto redirect user to add payment method
				self.redirectToCredit = true;
				self.showMpi();
			}
			else
			{
				$('.centerPar').removeClass('dontShow');
				$('#qrcode').addClass('withBorder');
				$('#scanText').addClass('bigLetter');
				
				//show the payment method select box!	
				$('#currPmethod , #whiteOne').show();
				
				//generate client code
				$('#qrcode').qrcode({
					color: '#000',
					text: clientCode ,
					label: clientCode , 
					mode: 2 , 
					fontcolor: 'FF9818' , 
					render: 'image' , 
					ecLevel: 'H' , 
					minVersion: 5
				});
				
				var isCoupon = false;
				$(json.PaymentMethods).each(function(i,v) 
				{
					if(v.type == 'Credit')
					{
						var isPref = (v.Preferred) ? ' (Default)' : '';
						select.append('<option data-ctype = "' + v.type + '" value="' + v.id + '" id = "opt' + v.id +'">' + v.name + ' Ending in ' + v.number + isPref + '</option>');
					}
					else if(v.type == 'Coupon')
					{
						isCoupon = true;
						v.id = 'coupon_1';
						select.append('<option data-ctype = "' + v.type + '" id = "opt' + v.id +'">Coupon - Amount $' + v.amount + '</option>');
						self.couponBalance += parseInt(v.amount);
					}
					
					//set default credit
					if(v.Preferred == true && !isCoupon)
						$('#opt'+v.id).attr('selected' , true);		
				});
				
				//check order... status should be "Closed"
				self.myTimer = window.setInterval(self.checkOrder, self.orderInterval);
			}
			
			//refresh the select box
			select.selectmenu('refresh', true);
			
			//set biz name
			$('.bizName').html(!json.BizName ? 'we work biz' : json.BizName);

            //tmp hack, replace logo for copilot cafe
            if(json.BizName.indexOf("Copilot") > -1 || json.BizName.indexOf("copilot") > -1) {
                $('.customHeader').addClass('copilot');

                $('.logoReplace').each(function(k,v) {
                    $(v).attr('src', './media/img/logo_coffee.jpg');
                    $(v).css({'width':'170px'});
                });
            }
			
			//set coupon balance
			$('.theBalance').html('$' + self.couponBalance);
		}
	};
	
	this.checkCredit = function()
	{
		console.log('credit card checked');
		
		if(self.redirectToCredit)
			self.showMpi();
	};	
	
	this.updatePrefMethod = function()
	{
		$('[data-ctype="Credit"]').each(function(i,v)
		{
			if(!$(v).prop('selected'))
				$(v).text($(v).text().replace(' (Default)' , ''));
			else
				$(v).text($(v).text() + ' (Default)');
		});
		
		//refresh the select box
		$('#paymentMethod').selectmenu('refresh', true);
		
		$.post(self.url + 'mobileApi/UpdatePrefMethod', { PrefPaymentMethod : $(this).find(":selected").attr('data-ctype') , CardID : $(this).val() }, function(json)  
		{
			if(json.ErrorCode == 0) 
			{ 
				//$(this).find(":selected").text($(this).find(":selected").text() + ' (Default)'); 
			}
		
		}, 'json');
		
		self.disableAdd = true;
	};
	
	this.backToNative = function()
	{
		console.log('back to native code..');
		window.location.reload();
	};
	
	this.checkVerify = function()
	{
		$.post(self.url + 'mobileApi/VerifyWework', {}, function(json) 
		{
			if(json.ErrorCode == 0)
				window.location.reload();
		}, 'json');	
	};
	
	this.checkOrder = function()
	{
		$.post(self.url + 'mobileApi/orderDetails', {'Quick': 1}, function(json) 
		{
			if(json.ErrorCode == 0)
			{
				if(json.Status == 'Closed')
				{
					$('#popupBasic').popup('open');
					window.clearInterval(self.myTimer);
					
					//jump to native code.. after 7 sec
					window.setTimeout(self.backToNative , 7000);
				}
				else if(json.Status == 'Cancelled')
				{
					$('#popupBasicCancel').popup('open');
					window.clearInterval(self.myTimer);
					
					//jump to native code.. after 7 sec
					window.setTimeout(self.backToNative , 7000);				
				}
			}	
		}, 'json');
	};
	
	this.doLogin = function()
	{
        if(self.getParameterByName('v2')) {
            var params = {
                'lat': self.getParameterByName('lat'),
                'lng': self.getParameterByName('lng')
            };
        } else {
            var params = { bid : self.getParameterByName('bid') };
        }

		var url = self.url + "mobileApi/GetWeworkCode";
		try { $.post(url, params, self.loginResult, 'json').error(self.loginError);	} catch (e) { console.log(e.message); }
	};
	
	this.showMpi = function()
	{
		//if(!self.disableAdd) return;
		$("body").pagecontainer("change", "#addCredit", { changeHash : true , transition : "slide"});
	};
	
	this.reloadMpi = function(e,a)
	{
		if(!self.mpiLoaded)
		{
			$('#popupLoader').popup('open');
			$('#mpiContainer').html('<iframe src = "' + self.mpiUrl + '" id = "mpiStyle" onload = "myApp.hidePopup()"></iframe>');
			self.mpiLoaded = true;
		}
		
		//enable card.io scanner, temporally disabled
		//try { JsAdapter.open('myApp.mpiStatus'); } catch (e) { console.log(e.message); }
	};
	
	this.mpiEnd = function(errorCode , errorMsg)
	{
		/* credit card added */
		if(errorCode == 0 || !errorCode)
		{
			$('#popupBasicPayment h3').html('Success!');
			$('#popupBasicPayment .thanksText').html('Credit card was added successfully!');
			$('#closePop').html('Continue');
			$(document).on('click' , '#closePop' , function() { window.location.reload(true); });
		}
		else
		{
			$('#popupBasicPayment h3').html('Error!');
			$('#popupBasicPayment .thanksText').html(errorMsg);
			$('#closePop').html('Try again!');
			$('#mpiStyle').attr('src' , self.mpiUrl);
			$(document).on('click' , '#closePop' , function() { $('#popupBasicPayment').popup('close'); });
		}
		
		$('#popupBasicPayment').popup('open');
	};
	
	this.hidePopup = function()
	{
		$('#popupLoader').popup('close');
	};
	
	this.mpiStatus = function(str)
	{
		var myJson = $.parseJSON(str);
		creditNumber =  myJson.Number;
	};
	
	this.setHeaders = function(appType)
	{
		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('AppType', appType);
			}
		});	
	};
	
	this.addCoupon = function()
	{
		if(!$('#promCode').val())
		{
			$('#couponStatus').html('').hide();
			return false;
		}
		
		$.post(self.url + 'mobileApi/Coupon', { Coupon : $('#promCode').val() }, function(json)  
		{
			if(json.ErrorCode != 0)
				$('#couponStatus').html(json.ReturnString).show();
			else
			{
				$('#couponStatus').html(json.ReturnString).addClass('green').show();
				window.setTimeout(self.backToNative , 3000);
			}
			
		}, 'json');
	};
	
	this.bindEvents = function()
	{
		//tap event
		$(document).on('tap' , '#addCreditBtn' , self.showMpi);
		$(document).on('tap' , '#addCouponBtn' , function() { $("body").pagecontainer("change", "#addCoupon", { changeHash : true , transition : "slide"}); });
		
		//page show event
		$(document).on("pageshow" , "#addCredit" , self.reloadMpi);
		$(document).on("pageshow" , function() { $(':submit').removeClass('loading'); } );
		$(document).on("pageshow" , "#qr" , self.checkCredit);
		
		//touch move event
		$(document).on('touchmove scrollstart', false);
		
		//click event
		$(document).on("click" , '#paymentMethod' , function() { self.disableAdd = false; } );
		$(document).on('click' , '.reloadPage' , function() { window.location.reload(true); });
		$(document).on('click' , '#addCouponSubmit' , self.addCoupon);
		$(document).on('click' , ':submit',function(e) { $(this).addClass('loading'); });
		
		//change event
		//$(document).on("blur" , '#paymentMethod' , function() { $("#btnList").show(); });
		//$(document).on("focus" , '#paymentMethod' , function() { $("#btnList").hide(); });
		$(document).on("change" , '#paymentMethod' , self.updatePrefMethod);
		
		//ajax complete
		$(document).ajaxComplete(function() {$(':submit').removeClass('loading');});
		
		//popup
		$("[data-role=popup]").on({popupbeforeposition: function () {$('.ui-popup-screen').off(); }});	
		//$("#addCoupon").on({popupafteropen: function () {$('#promCode').trigger('focus');}});
	};
	
	this.init = function()
	{
		//set headers
		self.setHeaders(self.appType);
		
		//bind events
		self.bindEvents();
		
		//do login.. and get code
		self.doLogin();
	};
}